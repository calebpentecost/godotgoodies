Everything I've done with GODOT will go here. Hopefully its portable!

Branch Organization:

  MASTER
    This will include basically everything -- as is tradition, pull requests are from other branches INTO this one
  
  SETUP
    These branches/commits were for adding the godot engine itself as well as the demo projects -- if you want a blank slate for godot here is a decent place to start
    This folder can be considered obsolete, as it was merged into master
    
  TUTORIAL
    I followed an online tutorial, and created different branches for every step of the way. There's a readme in that folder with more information
    This folder is not yet merged into MASTER, and to be honest it may never be (but feel free to pull the DONE branch if you want to see the end result

// End of file
